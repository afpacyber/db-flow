package fr.afpa.dbflow.database;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = AppDatabase.class)
public class User extends BaseModel implements Serializable {

    @PrimaryKey(autoincrement = true)
    public int id;

    @Column
    public String name;

    @Column
    public String pseudo;

    @Column
    public int age;
}