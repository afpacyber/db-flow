package fr.afpa.dbflow.ui.listing;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import fr.afpa.dbflow.R;
import fr.afpa.dbflow.database.User;
import fr.afpa.dbflow.database.User_Table;
import fr.afpa.dbflow.ui.main.MainActivity;
import fr.afpa.dbflow.utils.FastDialog;

public class ListingActivity extends AppCompatActivity {
    private ListView listViewUser;
    private UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        listViewUser = (ListView) findViewById(R.id.listViewUser);

        final List<User> userTableList = SQLite.select()
                .from(User.class)
                .queryList();

        adapter = new UserAdapter(ListingActivity.this, R.layout.item_user, userTableList);
        listViewUser.setAdapter(adapter);

        listViewUser.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                FastDialog.showDialog(ListingActivity.this,
                        FastDialog.SIMPLE_DIALOG,
                        "Êtes-vous sur ?",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                User user = userTableList.get(position);

                                // effacement dans la table User
                                // DELETE FROM user WHERE id = 1
                                SQLite.delete().from(User.class).where(User_Table.id.eq(user.id)).query();

                                // effacement dans la List<User> userTableList
                                userTableList.remove(position);

                                // rafrachissement des données
                                adapter.notifyDataSetChanged();
                            }
                        },
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                return true;
            }
        });

        listViewUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ListingActivity.this, MainActivity.class);

                intent.putExtra("user", userTableList.get(position));

                startActivity(intent);
            }
        });

//        for (User user : userTableList) {
//            Log.e("query", "user name: "+user.name);
//        }

    }
}
