package fr.afpa.dbflow.ui.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.afpa.dbflow.R;
import fr.afpa.dbflow.database.User;

public class MainActivity extends AppCompatActivity {

    private EditText inputName;
    private EditText inputAge;
    private EditText inputID;
    private Button inscriptionButton;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputName = (EditText) findViewById(R.id.inputName);
        inputAge = (EditText) findViewById(R.id.inputAge);
        inputID = (EditText) findViewById(R.id.inputID);
        inscriptionButton = (Button) findViewById(R.id.inscriptionButton);

        if(getIntent().getExtras() != null) {
            user = (User) getIntent().getExtras().get("user");

            inputName.setText(user.name);
            inputID.setText(user.pseudo);
            inputAge.setText(String.valueOf(user.age));
            inscriptionButton.setText("Enregistrer");
        }

        inscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // check name
                if(inputName.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Vous devez saisir le nom", Toast.LENGTH_LONG).show();
                    return;
                }

                // check age
                if(inputAge.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Vous devez saisir votre age", Toast.LENGTH_LONG).show();
                    return;
                }

                // check age numeric (java isnumeric sur stackoverflow)
                if(!isNumeric(inputAge.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Vous devez saisir votre age", Toast.LENGTH_LONG).show();
                    return;
                }

                // check age
                int age = Integer.valueOf(inputAge.getText().toString());
                if(age < 1 || age > 150) {
                    Toast.makeText(MainActivity.this, "Vous devez saisir votre age réel", Toast.LENGTH_LONG).show();
                    return;
                }

                User user1 = new User();
                user1.name = inputName.getText().toString();
                user1.age = Integer.valueOf(inputAge.getText().toString());
                user1.pseudo = inputID.getText().toString();

                if(user != null) {
                    user1.id = user.id;
                    if(user1.update()) {
                        inputName.getText().clear();
                        inputAge.setText(null);
                        inputID.setText("");

                        Toast.makeText(MainActivity.this, "Informations enregistrées", Toast.LENGTH_SHORT).show();
                        return;
                    }
                } else {
                    if(user1.save()) {
                        inputName.getText().clear();
                        inputAge.setText(null);
                        inputID.setText("");

                        Toast.makeText(MainActivity.this, "Informations enregistrées", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                Toast.makeText(MainActivity.this, "Erreur d'enregistrement", Toast.LENGTH_SHORT).show();
                return;

            }
        });
    }

    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }
}
