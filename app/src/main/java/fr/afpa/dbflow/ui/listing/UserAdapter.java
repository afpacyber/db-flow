package fr.afpa.dbflow.ui.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import fr.afpa.dbflow.R;
import fr.afpa.dbflow.database.User;

public class UserAdapter extends ArrayAdapter<User> {

    private int resId;

    public UserAdapter(Context context, int resource, List<User> objects) {
        super(context, resource, objects);

        resId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(resId, null);

        TextView textViewName = convertView.findViewById(R.id.textViewName);
        TextView textViewAge = convertView.findViewById(R.id.textViewAge);
        TextView textViewPseudo = convertView.findViewById(R.id.textViewPseudo);

        User item = getItem(position);

        textViewName.setText(item.name);
        textViewAge.setText(String.valueOf(item.age));
        textViewPseudo.setText(item.pseudo);

        return convertView;
    }
}
